object Assets {
  def image(path: String) = "/assets/images/" + path

  val clouds = image("clouds.png")
  val ship7b = image("ship7b.png")
  val ship8 = image("ship8.png")
  val ship1 = image("ship1.png")
  val blueBullet = image("blue_bullet.png")
  val greenBullet = image("green_bullet.png")
  val yellowBullet = image("yellow_bullet.png")
}
