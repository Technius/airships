import co.technius.scalajs.pixi._
import java.util.UUID
import scala.scalajs.js

import airships._

class RenderCache {
  private[this] val textures = js.Dictionary[Texture]()
  private[this] val cache = js.Dictionary[RenderData]()

  def apply(id: UUID): RenderData = cache(id.toString)

  def get(id: UUID): Option[RenderData] = cache.get(id.toString)

  def update(ctx: EntityRenderCtx, wCtx: WorldRenderCtx): (RenderData, Boolean) = {
    val id = ctx.entity.id.toString
    val res @ (data: RenderData, add: Boolean) = cache.get(id) match {
      case Some(x) => x -> false
      case None =>
        val s = ctx.renderable match {
          case r: Renderable.Rectangle =>
            val g = new Graphics
            g.beginFill(r.color, 1)
            g.drawRect(0, 0, wCtx.toCanvasX(ctx.size.width), wCtx.toCanvasY(ctx.size.height))
            new Sprite(g.generateTexture(100, Pixi.ScaleModes.Nearest))
          case r: Renderable.Sprite =>
            new Sprite(getTexture(r.filename))
        }
        val d = new RenderData(s)
        cache(id) = d
        d -> true
    }
    val s = data.sprite
    val c = data.container
    c.position.x = wCtx.toCanvasX(ctx.pos.x + wCtx.world.width / 2)
    c.position.y = wCtx.screenH - wCtx.toCanvasY(ctx.pos.y + wCtx.world.height / 2)
    ctx.renderable match {
      case r: Renderable.Sprite =>
        s.width = wCtx.toCanvasX(ctx.size.width * r.scale)
        s.height = wCtx.toCanvasY(ctx.size.height * r.scale)
      case _ =>
        s.width = wCtx.toCanvasX(ctx.size.width)
        s.height = wCtx.toCanvasY(ctx.size.height)
    }
    s.anchor.x = 0.5
    s.anchor.y = 0.5
    s.rotation = -ctx.pos.rotation
    res
  }

  def remove(ids: Seq[UUID]): Seq[RenderData] = ids flatMap { id =>
    cache.remove(id.toString)
  }

  def getTexture(file: String): Texture = textures.get(file) match {
    case Some(t) => t
    case None =>
      val t = Pixi.Texture.fromImage(file)
      textures(file) = t
      t
  }
}

class RenderData(private[this] var _sprite: Sprite) {
  val container = new Container
  container.addChild(_sprite)
  private[this] var _healthBar: Option[HealthBar] = None

  def healthBar: Option[HealthBar] = _healthBar

  def healthBar_=(s: HealthBar): Unit = {
    _healthBar foreach (h => container.removeChild(h.container))
    _healthBar = Some(s)
    container.addChild(s.container)
  }

  def sprite: Sprite = _sprite

  def sprite_=(s: Sprite): Unit = if (s != _sprite) {
    container.removeChild(_sprite)
    _sprite = s
    container.addChild(_sprite)
  }

  class HealthBar {
    val container = new Container
    private[this] val greenBar = genBar(0x00FF00)
    private[this] val redBar = genBar(0xFF0000)
    container.addChild(redBar)
    container.addChild(greenBar)

    def update(width: Double, height: Double, ratio: Double): Unit = {
      greenBar.width = width * ratio
      greenBar.height = height
      redBar.width = width
      redBar.height = height
    }

    private[this] def genBar(color: Int): Sprite = {
      val g = new Graphics
      g.beginFill(color, 1)
      g.drawRect(0, 0, 1, 1)
      new Sprite(g.generateTexture(1, Pixi.ScaleModes.Nearest))
    }
  }
}
