import co.technius.scalajs.pixi
import co.technius.scalajs.pixi.Pixi
import java.util.UUID
import org.scalajs.dom
import scala.collection.immutable.Seq

import airships._
import airships.{ Component => C }
import Conversions._

class RenderingSystem(renderer: pixi.SystemRenderer, camera: Camera, loader: AssetLoader, playerId: UUID) extends System {

  val cache = new RenderCache
  val wCtxMutable = WorldRenderCtxImpl(null, 0, 0)
  val eCtxMutable = new EntityRenderCtxImpl(wCtxMutable)

  val wCtx: WorldRenderCtx = wCtxMutable
  val eCtx: EntityRenderCtx = eCtxMutable

  val root = new pixi.Container()
  val viewport = camera.container
  val stage = new pixi.Container()
  val dynamicUi = new pixi.Container()
  val staticUi = new pixi.Container()
  val bg = Pixi.Sprite.fromImage(Assets.clouds)
  val youText = new pixi.Text("You", new pixi.TextStyle(fill = "white", font = "1.5em sans-serif"))
  val hpText = new pixi.Text("", new pixi.TextStyle(font = "1.5em sans-serif"))
  val fpsText = new pixi.Text("", new pixi.TextStyle(fill = "white", font = "1.5em sans-serif"))

  var lastFrame = 0.0

  init()

  def init(): Unit = {
    bg.position.x = 0
    bg.position.y = 0

    setupUi()

    viewport.addChild(bg)
    viewport.addChild(stage)
    viewport.addChild(dynamicUi)

    root.addChild(viewport)
    root.addChild(staticUi)
  }

  def setupUi(): Unit = {
    youText.anchor.x = 0.5
    youText.anchor.y = 0.5
    hpText.position.x = 10
    hpText.position.y = 10
    fpsText.position.x = 10
    fpsText.position.y = 30

    staticUi.addChild(hpText)
    staticUi.addChild(fpsText)

    dynamicUi.addChild(youText)
  }

  def apply(entities: Seq[Entity], world: World): Seq[Entity] = {
    dom.requestAnimationFrame { (time: Double) =>
      val delta = time - lastFrame
      val fps =
        if (delta == 0) Simulation.frequency
        else math.floor(1000.0 / delta)
      fpsText.text = fps + " FPS"
      render(world)
      lastFrame = time
    }
    entities
  }

  def render(world: World): Unit = {
    val lastWorld = if (wCtxMutable.world == null) world else wCtxMutable.world
    wCtxMutable.world = world
    wCtxMutable.screenW = renderer.view.width
    wCtxMutable.screenH = renderer.view.height

    camera.update(wCtx)

    val aspRatio = bg.texture.width.toDouble / bg.texture.height
    bg.width = aspRatio * wCtx.toCanvasX(world.width)
    bg.height = wCtx.toCanvasY(world.height)

    val existing = for {
      e <- world.entities
      r <- e.findComponent[C.Renderable]
      p <- e.findComponent[C.Position]
      s <- e.findComponent[C.Size]
    } yield {
      eCtxMutable.entity = e
      eCtxMutable.pos = p
      eCtxMutable.size = s
      eCtxMutable.healthOpt = e.findComponent[C.Health]
      eCtxMutable.renderable = r.renderable
      val (data, add) = cache.update(eCtx, wCtx)

      eCtx.healthOpt.filter(_.visible) foreach { hc =>
        val hb = data.healthBar match {
          case Some(x) => x
          case None =>
            val x = new data.HealthBar
            data.healthBar = x
            x
        }
        hb.container.x = -data.sprite.width / 2
        hb.container.y = -data.sprite.height / 2 - 10
        hb.update(data.sprite.width, 5, hc.health.toDouble / hc.maxHealth)
      }

      if (add) stage.addChild(data.container)

      if (e.id == playerId) {
        if (camera.chasePlayer) {
          camera.x = eCtx.pos.x
          camera.y = eCtx.pos.y
        }

        youText.position.x = wCtx.toCanvasX(eCtx.pos.x + world.width / 2)
        youText.position.y = wCtx.screenH - wCtx.toCanvasY(eCtx.pos.y + world.height / 2)

        eCtx.healthOpt foreach { hc =>
          if (hc.health > 0) {
            hpText.style.fill =
              if (hc.health > 50) "green"
              else if (hc.health > 25) "orange"
              else "red"
            hpText.text = s"${hc.health} / ${hc.maxHealth} health"
          } else {
            hpText.style.fill = "red"
            hpText.text = "gg rip"
          }
        }
      }

      e.id
    }

    cache.remove(lastWorld.entities.view.map(_.id).diff(existing)) foreach { d =>
      stage.removeChild(d.container)
    }

    renderer.render(root)
  }
}

sealed trait WorldRenderCtx {
  def world: World
  def screenW: Int
  def screenH: Int

  @inline def toCanvasX(x: Double): Double = worldx2Canvas(x, world, screenW)
  @inline def toCanvasY(y: Double): Double = worldy2Canvas(y, world, screenH)
}

case class WorldRenderCtxImpl(
  var world: World,
  var screenW: Int,
  var screenH: Int) extends WorldRenderCtx

sealed trait EntityRenderCtx {
  def wCtx: WorldRenderCtx
  def entity: Entity
  def size: C.Size
  def pos: C.Position
  def renderable: Renderable
  def healthOpt: Option[C.Health]
}

class EntityRenderCtxImpl(val wCtx: WorldRenderCtx) extends EntityRenderCtx {
  var entity: Entity = _
  var size: C.Size = _
  var pos: C.Position = _
  var renderable: Renderable = _
  var healthOpt: Option[C.Health] = _
}
