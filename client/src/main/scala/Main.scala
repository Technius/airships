import co.technius.scalajs.pixi
import org.scalajs.dom
import org.scalajs.dom.html
import java.util.UUID
import scala.collection.immutable.Seq
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js
import upickle.default._

import airships._
import airships.systems.InputSystem

@js.annotation.JSExportTopLevel("Main")
object Main {
  type Ctx2d = dom.CanvasRenderingContext2D

  @js.annotation.JSExport
  def main(online: Boolean): Unit = {
    val renderer = pixi.Pixi.autoDetectRenderer(640, 640)
    dom.document.body.appendChild(renderer.view)
    val loader = new AssetLoader

    val images = Seq(Assets.clouds, Assets.ship7b, Assets.ship8, Assets.ship1, Assets.yellowBullet)

    images foreach { img =>
      pixi.Pixi.loader.add(img)
    }

    pixi.Pixi.loader.load { (_: Any, _: Any) =>
      if (online) {
        remote(renderer, loader)
      } else {
        localSim(renderer, loader)
      }
    }
  }

  def remote(renderer: pixi.SystemRenderer, loader: AssetLoader): Unit = {
    val protocol = if (dom.location.protocol == "https:") "wss" else "ws"
    val url = s"$protocol://${dom.location.host}/connect"
    val ws = new dom.WebSocket(url)

    var handler: Protocol.Message => Any = null

    val initSession = (id: UUID) =>  {
      val camera = new Camera(0, 0, 100, 100)
      val render = new RenderingSystem(renderer, camera, loader, id)
      val inputMethod = new ClientInput(renderer.view, camera, id)
      val input = new InputSystem(inputMethod)
      val sim = new Simulation((input +: Simulation.defaultSystems) :+ render)

      var worldReady = false
      var lastUpdate = System.currentTimeMillis()
      dom.window.setInterval(() => if (worldReady) {
        val cmds = inputMethod.currentCommands(id)
        if (!cmds.isEmpty) ws.send(write(cmds))
        val now = System.currentTimeMillis()
        if (now - lastUpdate >= Simulation.period) {
          inputMethod.world = sim.simulate(inputMethod.world)
          lastUpdate = now
        }
      }, Simulation.period / 4)

      (m: Protocol.Message) => m match {
        case Protocol.UpdateWorld(world) =>
          inputMethod.world = world
          lastUpdate = System.currentTimeMillis()
          worldReady = true
          render(world.entities, world)
        case _ =>
      }
    }

    val preLogin = (m: Protocol.Message) => m match {
      case Protocol.UserAcknowledged(id) =>
        println("logged in")
        handler = initSession(id)
      case _ =>
    }

    handler = preLogin

    ws.onmessage = { (e: dom.MessageEvent) =>
      val msg = read[Protocol.Message](e.data.asInstanceOf[String])
      handler(msg)
    }

    ws.onclose = { (_: Any) =>
      dom.window.alert("Lost connection")
    }
  }

  def localSim(renderer: pixi.SystemRenderer, loader: AssetLoader): Unit = {
    val player = Entity(UUID.randomUUID(), Templates.player(0, 0))
    val block = Entity(UUID.randomUUID(), Templates.target(-40, 40))
    val bigguy = Entity(UUID.randomUUID(), Templates.dummyShip(20, 30))

    val camera = new Camera(0, 0, 100, 100)
    val inputMethod = new ClientInput(renderer.view, camera, player.id)
    val input = new InputSystem(inputMethod)
    val render = new RenderingSystem(renderer, camera, loader, player.id)
    val sim = new Simulation((input +: Simulation.defaultSystems) :+ render)

    def defaultWorld = World(100, 100, Seq(player, block, bigguy))
    var world = defaultWorld
    var paused = false

    dom.document.body.addEventListener("keydown", (e: dom.KeyboardEvent) => {
      import dom.ext.KeyCode
      e.keyCode match {
        case KeyCode.Backspace =>
          world = defaultWorld
        case KeyCode.P =>
          paused = !paused
        case _ =>
      }
    })

    var lastUpdate = System.currentTimeMillis()

    dom.window.setInterval(() => if (!paused) {
      val now = System.currentTimeMillis()
      if (now - lastUpdate >= Simulation.period) {
        inputMethod.world = world
        val newWorld = sim.simulate(world)
        world = newWorld
        lastUpdate = now
      }
    }, Simulation.period / 4)
  }
}
