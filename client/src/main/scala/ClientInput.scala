import org.scalajs.dom
import java.util.UUID
import scala.collection.immutable.Seq

import airships.World
import airships.systems.{ Input, InputKey }
import Conversions._

class ClientInput(canvas: dom.html.Canvas, camera: Camera, playerId: UUID) extends Input {

  var world: World = _

  var lmbDown = false
  var lastMouseDown = lmbDown
  var mousePos: (Double, Double) = null

  var keysDown = Set.empty[String]
  var lastKeysDown = keysDown

  var lastInput = Seq[InputKey]()

  canvas.addEventListener("contextmenu", (e: dom.MouseEvent) => e.preventDefault())

  canvas.addEventListener("mousedown", (e: dom.MouseEvent) => {
    if (e.button == 0) {
      lastMouseDown = lmbDown
      lmbDown = true
    }
  })

  canvas.addEventListener("mouseup", (e: dom.MouseEvent) => {
    if (e.button == 0) {
      lastMouseDown = lmbDown
      lmbDown = false
    }
  })

  dom.document.addEventListener("keydown", (e: dom.KeyboardEvent) => {
    lastKeysDown = keysDown
    keysDown = keysDown + e.key
  })

  dom.document.addEventListener("keyup", (e: dom.KeyboardEvent) => {
    lastKeysDown = keysDown
    keysDown = keysDown - e.key
  })

  dom.document.addEventListener("keypress", (e: dom.KeyboardEvent) => {
    var actualCommand = true
    e.key match {
      case " " =>
        camera.chasePlayer = !camera.chasePlayer
      case "+" =>
        camera.width = camera.width + 3
        camera.height = camera.height + 3
      case "-" =>
        camera.width = camera.width - 3
        camera.height = camera.height - 3
      case _ =>
        actualCommand = false
    }

    if (actualCommand) {
      e.preventDefault()
    }
  })

  canvas.addEventListener("mousemove", (e: dom.MouseEvent) => {
    val rect = canvas.getBoundingClientRect()
    val (cw, ch) = (canvas.width, canvas.height)
    // TODO: Adjust for camera
    val x = canvasx2World(e.clientX - rect.left - cw / 2, world.width, cw)
    val y = canvasy2World(ch / 2 - (e.clientY - rect.top), world.height, ch)
    mousePos = (x, y)
  }, false)

  canvas.addEventListener("wheel", (e: dom.WheelEvent) => {
    camera.width = camera.width + e.deltaY
    camera.height = camera.height + e.deltaY
    e.preventDefault()
  }, false)

  def currentCommands(id: UUID): Seq[InputKey] = {
    if (id == playerId) {
      var i = fetchInput()
      val same = lastInput.collect {
        case t: InputKey.SetTarget => t
        case m: InputKey.Move => m
      }
      val res = i.diff(same)
      lastInput = i
      res
    } else {
      Seq.empty
    }
  }

  def fetchInput(): Seq[InputKey] = {
    val target =
      if (mousePos == null) {
        Seq.empty
      } else if (!lmbDown) {
        Seq(InputKey.SetTarget(None))
      } else {
        val scaleX = camera.width / world.width
        val scaleY = camera.height / world.height
        val adjPos = (camera.x + mousePos._1 * scaleX, camera.y + mousePos._2 * scaleY)
        Seq(InputKey.SetTarget(Some(adjPos)))
      }

    val move = {
      val m = checkMove(keysDown)
      if (m == checkMove(lastKeysDown)) Seq.empty else Seq(m)
    }
    target ++ move
  }

  def checkMove(keys: Set[String]): InputKey.Move = {
    val up    = keys.contains("w")
    val left  = keys.contains("a")
    val down  = keys.contains("s")
    val right = keys.contains("d")
    
    val x: Option[Boolean] = if (left && !right) Some(false) else if (right && !left) Some(true) else None
    val y: Option[Boolean] = if (up && !down) Some(true) else if (down && !up) Some(false) else None
    InputKey.Move(x, y)
  }
}
