import airships.{ geom, World }

object Conversions {
  @inline def worldx2Canvas(x: Double, world: World, cw: Double): Double =
    (x * cw) / world.width

  @inline def worldy2Canvas(y: Double, world: World, ch: Double): Double =
    (y * ch) / world.height

  @inline def canvasx2World(x: Double, world: World, cw: Double): Double =
    canvasx2World(x, world.width, cw)

  @inline def canvasy2World(y: Double, world: World, ch: Double): Double =
    canvasy2World(y, world.height, ch)

  @inline def canvasx2World(x: Double, ww: Double, cw: Double): Double =
    geom.scale(x, cw, ww)

  @inline def canvasy2World(y: Double, wh: Double, ch: Double): Double =
    geom.scale(y, ch, wh)
}
