import co.technius.scalajs.pixi
import scala.scalajs.js

class Camera(
    var x: Double,
    var y: Double,
    var width: Double,
    var height: Double) {

  val container = new pixi.Container()

  var chasePlayer = false

  def update(ctx: WorldRenderCtx): Unit = {
    // TODO: Is this an optimization?
    if (ctx.world.width == width && ctx.world.height == height) {
      container.pivot.x = 0
      container.pivot.y = 0
      container.position.x = -ctx.toCanvasX(x)
      container.position.y = ctx.toCanvasY(y)
    } else {
      val scaleX = ctx.world.width / width
      val scaleY = ctx.world.height / height
      container.pivot.x = ctx.screenW / 2
      container.pivot.y = ctx.screenH / 2
      container.position.x = container.pivot.x - ctx.toCanvasX(x) * scaleX
      container.position.y = container.pivot.y + ctx.toCanvasY(y) * scaleY
      container.scale.x = scaleX
      container.scale.y = scaleY
    }
  }
}
