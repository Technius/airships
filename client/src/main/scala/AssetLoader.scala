import scala.concurrent.{ ExecutionContext, Future, Promise }
import org.scalajs.dom
import org.scalajs.dom.html.Image

class AssetLoader {
  private[this] val cache = scala.collection.mutable.Map[String, Image]()

  def get(path: String): Option[Image] = cache.get(path)

  def loadImage(path: String): Future[Image] = cache.get(path) match {
    case Some(img) =>
      Future.successful(img)
    case None =>
      val img = dom.document.createElement("img").asInstanceOf[Image]
      val p = Promise[Image]
      img.onload = (e: dom.Event) => {
        cache(path) = img
        p success img
      }
      img.addEventListener("error", (e: dom.Event) => {
        p failure new Exception("Failed to load image")
      })
      img.src = path
      p.future
  }

  def loadImages(paths: String*)(implicit ec: ExecutionContext): Future[Seq[Image]] =
    Future.sequence(paths map loadImage)
}
