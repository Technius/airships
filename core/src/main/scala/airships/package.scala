import scala.collection.immutable.Seq

package object airships {
  type System = (Seq[Entity], World) => Seq[Entity]
}
