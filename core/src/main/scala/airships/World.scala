package airships

import scala.collection.immutable.Seq

import java.util.UUID

case class World(width: Double, height: Double, entities: Seq[Entity]) {
  def apply(id: UUID): Option[Entity] = entities.find(_.id == id)
}
