package airships

import scala.collection.immutable.Seq

import systems._

class Simulation(systems: Seq[System]) {
  def simulate(world: World): World = {
    val res = systems.foldLeft(world.entities)((acc, f) => f(acc, world))
    world.copy(entities = res)
  }
}

object Simulation {
  val defaultSystems = Seq[System](
    MovementSystem,
    BoundaryRemoveSystem,
    ShootingSystem,
    CollisionDamageSystem,
    HealthCheckSystem)

  val frequency = 60
  val period = 1000.0 / frequency
}
