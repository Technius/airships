package airships.geom

object Intersections {
  @inline def intersects(a: Rectangle, b: Rectangle): Boolean =
    a.left < b.right && a.right > b.left && a.bottom < b.top && a.top > b.bottom
}
