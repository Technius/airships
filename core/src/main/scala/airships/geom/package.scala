package airships

package object geom {
  @inline def clamp(x: Double, min: Double, max: Double): Double =
    if (x > max) max
    else if (x < min) min
    else x

  @inline def dist(x1: Double, y1: Double, x2: Double, y2: Double): Double =
    dist(x1 - x2, y1 - y2)

  @inline def dist(dx: Double, dy: Double): Double =
    math.sqrt(dx * dx + dy * dy)

  /**
   * Given a, b, and c, returns c in a / b = c / d
   */
  @inline def scale(a: Double, b: Double, d: Double): Double =
    (a * d) / b

  val sqrtOf2 = math.sqrt(2)
}
