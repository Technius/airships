package airships.geom

case class Rectangle(x1: Double, y1: Double, x2: Double, y2: Double) {
  lazy val left: Double = math.min(x1, x2)
  lazy val right: Double = math.max(x1, x2)
  lazy val top: Double = math.max(y1, y2)
  lazy val bottom: Double = math.min(y1, y2)
  lazy val max: (Double, Double) = (right, top)
  lazy val min: (Double, Double) = (left, bottom)
  lazy val width: Double = math.abs(x1 - x2)
  lazy val height: Double = math.abs(y1 - y2)

  @inline def translate(x: Double, y: Double): Rectangle =
    Rectangle(x1 + x, y1 + y, x2 + x, y2 + y)
}

object Rectangle {
  @inline def fromCoords(x: Double, y: Double, width: Double, height: Double): Rectangle =
    Rectangle(x + width/2, y + height/2, x - width/2, y - height/2)
}
