package airships

import java.util.UUID
import scala.collection.immutable.Seq
import scala.language.experimental.macros

import impl.Macros

case class Entity(id: UUID, components: Seq[Component]) {
  def findComponent[T <: Component]: Option[T] = macro Macros.findComponentImpl[T]

  def replacingComponent(old: Component, upd: Component) =
    copy(components = components.filterNot(_ == old) :+ upd)
}

sealed trait Component
object Component {
  case class Position(
    x: Double,
    y: Double,
    speed: Double,
    rotation: Double = 0,
    vx: Double = 0,
    vy: Double = 0) extends Component
  case class Size(width: Double, height: Double) extends Component
  case class Renderable(renderable: airships.Renderable) extends Component
  case class Projectile(
    cooldown: Int,
    speed: Double,
    data: Seq[Component],
    target: Option[(Double, Double)] = None,
    lastFired: Int = 0) extends Component
  case class Health(health: Int, maxHealth: Int, visible: Boolean = true) extends Component
  case class Damage(damage: Int) extends Component
  case class Fragile(damage: Int) extends Component
  case class OwnedBy(owner: UUID) extends Component
  case object BoundaryDeath extends Component
  case object Unkillable extends Component
  case object RotateWithVelocity extends Component
}
