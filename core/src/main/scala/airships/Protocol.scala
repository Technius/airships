package airships

import java.util.UUID

object Protocol {
  sealed trait Message
  case class UpdateWorld(world: World) extends Message
  case class UserAcknowledged(id: UUID) extends Message
}
