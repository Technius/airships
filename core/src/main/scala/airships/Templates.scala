package airships

import scala.collection.immutable.Seq
import airships.{ Component => C }

object Templates {
  def player(x: Double, y: Double): Seq[Component] =
    Seq(
      C.Position(x, y, 1, rotation = math.toRadians(90)),
      C.RotateWithVelocity,
      C.Size(10, 10),
      C.Health(100, 100),
      C.Unkillable,
      C.Projectile(10, 2, Seq(
        C.Size(2, 2),
        C.Damage(1),
        C.Health(1, 1, visible = false),
        C.Fragile(1),
        C.Renderable(Renderable.Sprite("/assets/images/yellow_bullet.png", scale = 3))
      )),
      C.Renderable(Renderable.Sprite("/assets/images/ship7b.png"))
    )

  def target(x: Double, y: Double): Seq[Component] =
    Seq(
      C.Position(x, y, 0, rotation = math.toRadians(90)),
      C.Size(5, 5),
      C.Health(10, 10),
      C.Renderable(Renderable.Sprite("/assets/images/ship8.png"))
    )

  def spawner(x: Double, y: Double, proj: C.Projectile): Seq[Component] =
    Seq(
      C.Position(x, y, 0),
      proj
    )

  def dummyShip(x: Double, y: Double): Seq[Component] =
    Seq(
      C.Position(x, y, 0, rotation = math.toRadians(90)),
      C.Size(30, 30),
      C.Health(20, 20),
      C.Renderable(Renderable.Sprite("/assets/images/ship1.png"))
    )
}
