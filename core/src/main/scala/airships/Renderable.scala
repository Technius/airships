package airships

sealed trait Renderable

object Renderable {
  case class Rectangle(color: Int) extends Renderable
  case class Sprite(filename: String, scale: Double = 1) extends Renderable
}
