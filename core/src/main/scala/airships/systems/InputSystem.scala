package airships.systems

import scala.collection.immutable.Seq
import java.util.UUID

import airships._
import airships.{ Component => C }

class InputSystem(inputMethod: Input) extends System {
  def apply(entities: Seq[Entity], world: World): Seq[Entity] = {
    entities map { e =>
      inputMethod.currentCommands(e.id).foldLeft(e)(applyCommand)
    }
  }

  def applyCommand(e: Entity, k: InputKey): Entity = k match {
    case InputKey.MoveTo(x, y) =>
      val opt = for (p <- e.findComponent[C.Position]) yield {
        val (dx, dy) = (x - p.x, y - p.y)
        val dist = geom.dist(dx, dy)
        val (vx, vy) =
          if (dist <= p.speed) (dx, dy)
          else (geom.scale(dx, dist, p.speed), geom.scale(dy, dist, p.speed))

        val upd = p.copy(vx = vx, vy = vy)
        e.copy(components = e.components.filterNot(_ == p) :+ upd)
      }
      opt getOrElse e
    case InputKey.Move(x, y) =>
      val opt = for (p <- e.findComponent[C.Position]) yield {
        val diagSpd = p.speed / geom.sqrtOf2
        val (vx, vy) = (x, y) match {
          case (None, None) => (0.0, 0.0)
          case (Some(true), Some(true)) => (diagSpd, diagSpd)
          case (Some(false), Some(false)) => (-diagSpd, -diagSpd)
          case (Some(true), Some(false)) => (diagSpd, -diagSpd)
          case (Some(false), Some(true)) => (-diagSpd, diagSpd)
          case (Some(true), None) => (p.speed, 0.0)
          case (Some(false), None) => (-p.speed, 0.0)
          case (None, Some(true)) => (0.0, p.speed)
          case (None, Some(false)) => (0.0, -p.speed)
        }
        val upd = p.copy(vx = vx, vy = vy)
        e.copy(components = e.components.filterNot(_ == p) :+ upd)
      }
      opt getOrElse e
    case InputKey.SetTarget(target) =>
      var opt = for (p <- e.findComponent[C.Projectile]) yield {
        val upd = p.copy(target = target)
        e.copy(components = e.components.filterNot(_ == p) :+ upd)
      }
      opt getOrElse e
  }
}

trait Input {
  def currentCommands(id: UUID): Seq[InputKey]
}

sealed trait InputKey

object InputKey {
  case class MoveTo(x: Double, y: Double) extends InputKey
  case class Move(x: Option[Boolean], y: Option[Boolean]) extends InputKey
  case class SetTarget(target: Option[(Double, Double)]) extends InputKey
}
