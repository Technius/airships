package airships.systems

import scala.collection.immutable.Seq

import airships._
import airships.{ Component => C }

object BoundaryRemoveSystem extends System {
  def apply(entities: Seq[Entity], world: World): Seq[Entity] = {
    val halfW = world.width / 2
    val halfH = world.height / 2
    entities flatMap { e =>
      e.findComponent[C.Position] match {
        case Some(p) =>
          if (math.abs(p.x) > halfW || math.abs(p.y) > halfW) {
            val upd = p.copy(
              x = geom.clamp(p.x, -halfW, halfW),
              y = geom.clamp(p.y, -halfH, halfH)
            )
            if (e.components.contains(C.BoundaryDeath)) None
            else Some(e.replacingComponent(p, upd))
          } else Some(e)
        case None => Some(e)
      }
    }
  }
}
