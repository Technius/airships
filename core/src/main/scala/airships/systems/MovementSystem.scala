package airships.systems

import scala.collection.immutable.Seq

import airships._
import airships.{ Component => C }

object MovementSystem extends System {
  def apply(entities: Seq[Entity], world: World): Seq[Entity] = entities map { e =>
    val opt = for {
      p <- e.findComponent[C.Position]
    } yield {
      val x = p.x + p.vx
      val y = p.y + p.vy
      val rotation =
        if (e.components.contains(C.RotateWithVelocity) && (p.vx != 0 || p.vy != 0)) math.atan2(p.vy, p.vx)
        else p.rotation
      val upd = p.copy(x = x, y = y, rotation = rotation)
      e.replacingComponent(p, upd)
    }

    opt getOrElse e
  }
}
