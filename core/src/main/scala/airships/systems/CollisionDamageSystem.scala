package airships.systems

import scala.collection.immutable.Seq
import java.util.UUID

import airships._
import airships.geom.{ Intersections, Rectangle }
import airships.{ Component => C }

object CollisionDamageSystem extends System {
  private[this] type Damageable = (Entity, Rectangle)
  def apply(entities: Seq[Entity], world: World): Seq[Entity] = {
    val (ign, candidates) = entities.foldLeft(Seq.empty[Entity], Seq.empty[Damageable]) { (acc, e) =>
      val opt = for {
        p <- e.findComponent[C.Position]
        s <- e.findComponent[C.Size]
      } yield {
        (e, Rectangle.fromCoords(p.x, p.y, s.width, s.height))
      }
      opt match {
        case Some(d) => (acc._1, acc._2 :+ d)
        case None => (acc._1 :+ e, acc._2)
      }
    }

    val collided = candidates map { a =>
      import Intersections.intersects
      
      val (e, bounds) = a
      val ownedByOpt = e.findComponent[C.OwnedBy]
      val ownerFilter: Entity => Boolean = { other: Entity =>
        val otherOb = other.findComponent[C.OwnedBy]
        ownedByOpt
          .map(_.owner != other.id)
          .orElse(otherOb.map(_.owner != e.id))
          .getOrElse(true)
      }
      e.findComponent[C.Health] match {
        case Some(h) =>
          val dmgCollisions =
            candidates
              .filter(b => b != a && intersects(bounds, b._2))
              .filter(b => ownerFilter(b._1))

          val collideDmg =
              dmgCollisions
              .view
              .flatMap(_._1.findComponent[C.Damage])
              .map(_.damage)
              .sum

          val fragileDmg =
            e.findComponent[C.Fragile].map(_.damage).getOrElse(0)

          val dmg = collideDmg + fragileDmg * dmgCollisions.length

          if (dmg > 0) {
            val upd = h.copy(health = h.health - dmg)
            e.replacingComponent(h, upd)
          } else {
            e
          }
        case None =>
          e
      }
    }
    ign ++ collided
  }
}
