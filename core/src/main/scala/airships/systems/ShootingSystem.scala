package airships.systems

import scala.collection.immutable.Seq

import airships._
import airships.{ Component => C }

object ShootingSystem extends System {
  def apply(entities: Seq[Entity], world: World): Seq[Entity] = {
    entities flatMap { e =>
      val opt: Option[Seq[Entity]] = for {
        pos <- e.findComponent[C.Position]
        proj <- e.findComponent[C.Projectile]
      } yield {

        if (proj.lastFired > 0) {
          Seq(e.replacingComponent(proj, proj.copy(lastFired = proj.lastFired - 1)))
        } else proj.target match {
          case Some((tx, ty)) if tx != pos.x || ty != pos.y =>
            val (dx, dy) = (tx - pos.x, ty - pos.y)
            val dist = geom.dist(dx, dy)
            val (vx, vy) = (geom.scale(dx, dist, proj.speed), geom.scale(dy, dist, proj.speed))
            val rotation = math.atan2(vy, vx)
            val projPos = C.Position(pos.x, pos.y, proj.speed, rotation = rotation, vx = vx, vy = vy)
            val ownedBy = C.OwnedBy(e.id)
            val comp = Seq(projPos, ownedBy, C.BoundaryDeath)
            Seq(
              e.replacingComponent(proj, proj.copy(lastFired = proj.cooldown)),
              Entity(java.util.UUID.randomUUID(), comp ++ proj.data)
            )
          case _ => Seq(e)
        }
      }
      opt getOrElse Seq(e)
    }
  }
}
