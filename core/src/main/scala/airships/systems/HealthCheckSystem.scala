package airships.systems

import scala.collection.immutable.Seq

import airships._
import airships.{ Component => C }

object HealthCheckSystem extends System {
  def apply(entities: Seq[Entity], world: World): Seq[Entity] =
    entities flatMap { e =>
      e.findComponent[C.Health] match {
        case Some(h) if h.health <= 0 && !e.components.contains(C.Unkillable) =>
          None
        case _ => Some(e)
      }
    }
}
