name := """airships"""

version := "0.0.1-SNAPSHOT"

lazy val sharedSettings = Seq(
  scalaVersion := "2.11.8",
  libraryDependencies += "com.lihaoyi" %%% "upickle" % "0.4.1",
  scalacOptions ++= Seq(
    "-feature",
    "-deprecation",
    "-Xlint",
    "-Xfatal-warnings",
    "-Xfuture",
    "-Yno-adapted-args"
  )
)

lazy val root = (project in file(".")).aggregate(client, server)

lazy val core =
  crossProject.crossType(CrossType.Pure).in(file("core"))
    .settings(sharedSettings: _*)
    .jsConfigure(_.enablePlugins(ScalaJSWeb).dependsOn(macros))
    .jvmConfigure(_.dependsOn(macros))

lazy val coreJs = core.js

lazy val coreJvm = core.jvm

lazy val macros =
  (project in file("macros"))
    .settings(sharedSettings: _*)
    .settings(
      libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
    )

lazy val server =
  (project in file("server"))
    .settings(sharedSettings: _*)
    .settings(
      name := "airships-server",
      libraryDependencies ++= Seq(
        jdbc,
        cache,
        "org.webjars" %% "webjars-play" % "2.5.0-3",
        "org.webjars" % "pixi.js" % "3.0.7"
      ),
      routesGenerator := InjectedRoutesGenerator,
      scalaJSProjects := Seq(client),
      pipelineStages in Assets := Seq(scalaJSDev),
      pipelineStages := Seq(scalaJSProd, digest, gzip)
    )
    .enablePlugins(PlayScala)
    .aggregate(client)
    .dependsOn(coreJvm)

lazy val client =
  (project in file("client"))
    .settings(sharedSettings: _*)
    .settings(
      name := "airships-client",
      resolvers += "Artifactory" at "https://oss.jfrog.org/artifactory/oss-snapshot-local/",
      libraryDependencies ++= Seq(
        "org.scala-js" %%% "scalajs-dom" % "0.8.2",
        "co.technius.scalajs-pixi" %%% "core" % "0.0.1-SNAPSHOT"
      )
    )
    .enablePlugins(ScalaJSPlugin, ScalaJSWeb)
    .dependsOn(coreJs)
