package actors

import akka.actor.{ Actor, ActorRef, Props, Terminated }
import java.util.UUID
import scala.collection.immutable.Seq

import airships._
import airships.{ Component => C }
import airships.systems.InputSystem

class ServerActor extends Actor {
  val connectionInput = new ConnectionInput
  var sim = new Simulation(new InputSystem(connectionInput) +: Simulation.defaultSystems)

  var world = World(100, 100, Seq(
    Entity(UUID.randomUUID(), Templates.spawner(0, 50, C.Projectile(
      120, 0.5, Templates.target(0, 0) flatMap { case p: C.Position => None; case x => Some(x) },
      target = Some((0, -50))
    )))
  ))
  var skippedUpdates = 0
  val users = collection.mutable.ArrayBuffer.empty[(UUID, ActorRef)]
  val spawnQueue = collection.mutable.Queue.empty[Entity]
  val deathQueue = collection.mutable.Queue.empty[UUID]

  def receive = {
    case Message.RunSimulation =>
      val spawned = spawnQueue.dequeueAll(_ => true)
      val killed = deathQueue.dequeueAll(_ => true)
      val entities = (world.entities ++ spawned).filterNot(e => killed.contains(e.id))
      val newWorld = sim.simulate(world.copy(entities = entities))
      if (world != newWorld) {
        world = newWorld
        if (skippedUpdates == 1) {
          skippedUpdates = 0
        } else {
          skippedUpdates = skippedUpdates + 1
        }
        users foreach { kp =>
          val id = kp._1
          val justSpawned = spawned.exists(_.id == id)
          if (justSpawned) kp._2 ! Protocol.UserAcknowledged(id)
          if (skippedUpdates == 0 || justSpawned) kp._2 ! Protocol.UpdateWorld(world)
        }
      } else if (skippedUpdates == 1) {
        skippedUpdates = 0
        users foreach (_._2 ! Protocol.UpdateWorld(world))
      }
    case Message.QueueInput(k) => users.find(_._2 == sender()) match {
        case Some((id, _)) => connectionInput.queue(id, k)
        case _ =>
      }
    case Message.Connect =>
      val ent = Entity(UUID.randomUUID(), Templates.player(0, 0))
      users += (ent.id -> sender())
      context.watch(sender())
      spawnQueue += ent
    case Terminated(ref) =>
      users.find(_._2 == ref) foreach { kp =>
        users -= kp
        deathQueue += kp._1
      }
  }
}

object ServerActor {
  def props = Props(new ServerActor)
}
