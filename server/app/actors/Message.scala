package actors

import airships.systems.InputKey

sealed trait Message
object Message {
  case object RunSimulation extends Message
  case object Connect extends Message
  case object Disconnect extends Message
  case class QueueInput(key: InputKey) extends Message
}
