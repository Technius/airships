package actors

import java.util.UUID
import scala.collection.mutable.{ Queue => MutableQueue, Map => MutableMap }
import scala.collection.immutable.Seq

import airships.systems.{ Input, InputKey }

class ConnectionInput extends Input {
  private[this] val _queue = MutableMap.empty[UUID, MutableQueue[InputKey]]
  def queue(id: UUID, key: InputKey): Unit = {
    _queue.get(id) match {
      case Some(b) => b += key
      case None =>
        val q = MutableQueue[InputKey](key)
        _queue(id) = q
    }
  }

  def currentCommands(id: UUID): Seq[InputKey] = _queue.get(id) match {
    case Some(b) => b.dequeueAll(_ => true).toList
    case None => Seq.empty
  }
}
