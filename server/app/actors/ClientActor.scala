package actors

import akka.actor.{ Actor, ActorRef, Props }
import scala.util.Try
import upickle.default._

import airships.Protocol
import airships.systems.InputKey

class ClientActor(out: ActorRef, server: ActorRef) extends Actor {
  override def preStart() = {
    server ! Message.Connect
  }

  def receive = {
    case msg: Protocol.Message =>
      out ! write(msg)
    case s: String =>
      Try(read[Seq[InputKey]](s)).toOption foreach { k =>
        k foreach handleMessage
      }
  }

  def handleMessage(msg: InputKey) = server ! Message.QueueInput(msg)
}

object ClientActor {
  def props(out: ActorRef, server: ActorRef) = Props(new ClientActor(out, server))
}
