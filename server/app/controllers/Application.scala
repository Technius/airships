package controllers

import akka.actor.ActorSystem
import akka.stream.Materializer
import play.api._
import play.api.mvc._
import play.api.libs.streams._
import javax.inject.Inject
import scala.concurrent.duration._

class Application @Inject()(webJarAssets: WebJarAssets,
                            environment: Environment)
                           (implicit system: ActorSystem,
                            mat: Materializer) extends Controller {

  def index = Action {
    Ok(views.html.index(true)(environment, webJarAssets))
  }

  def singlePlayer = Action {
    Ok(views.html.index(false)(environment, webJarAssets))
  }

  import play.api.Play.current

  val gameServer = system.actorOf(actors.ServerActor.props)

  import play.api.libs.concurrent.Execution.Implicits._
  system.scheduler.schedule(0.milliseconds, (1000.0 / 60.0).milliseconds,
    gameServer, actors.Message.RunSimulation)

  def connect = WebSocket.accept[String, String] { request =>
    ActorFlow.actorRef(out => actors.ClientActor.props(out, gameServer))
  }

}
