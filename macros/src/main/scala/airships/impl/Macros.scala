package airships.impl

import scala.reflect.macros.blackbox.Context

object Macros {
  def findComponentImpl[T: c.WeakTypeTag](c: Context): c.Expr[Option[T]]  = {
    import c.universe._
    val typ = weakTypeOf[T]
    val thisRef = c.prefix
    val tree = q"$thisRef.components.collectFirst { case x: $typ => x}"
    c.Expr[Option[T]](tree)
  }
}
